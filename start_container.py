# -*- coding: utf-8 -*-
# created by lilei at 2020/9/28
import sys
import logging

import docker
from docker.errors import NotFound
from docker.models.containers import Container

client = docker.from_env()
logging.basicConfig(level=logging.INFO)


def _login(du_name: str, d_password: str):
    """登录"""
    logging.info('登录...')
    client.login(du_name, d_password)
    logging.info('登录成功...')


def execute(
        du_name: str,
        d_password: str,
        project_name: str,
        host_port: int,
        container_port: int,
        host_dir: str = None,
        container_dir: str = None
):
    logging.info('开始执行...')
    host_port = int(host_port)
    container_port = int(container_port)
    # 登录
    _login(du_name, d_password)
    # 镜像名
    image_name = f'{du_name}/{project_name}'
    # 如果已存在该镜像, 移除
    try:
        container: Container = client.containers.get(project_name)
        logging.warning(f'容器{project_name}已存在, 停止并移除...')
        container.stop()
        container.remove()
        logging.info('停止移除源容器成功...')
    except NotFound:
        pass
    # 启动容器
    start_kwargs = {
        'name': project_name,
        'ports': {host_port: container_port},
    }
    if all([host_dir, container_dir]):
        start_kwargs['volumes'] = {
            host_dir: {'bind': container_dir, 'mode': 'ro'}
        }
    try:
        client.images.get(image_name)
        client.images.remove(image_name, force=True)
        logging.info(f'删除本地镜像: {image_name}...')
    except NotFound:
        pass
    logging.info(f'拉取远程镜像: {image_name}...')
    client.images.pull(image_name + ':latest')
    logging.info('启动容器...')
    client.containers.run(
        image_name,
        detach=True,
        **start_kwargs
    )


if __name__ == '__main__':
    params = sys.argv[1:]
    if len(params) not in [5, 7]:
        error_str = 'python script params error.'
        logging.error(error_str)
        raise SystemError(error_str)
    logging.info(f'参数: {params}')
    execute(*params)
