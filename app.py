# -*- coding: utf-8 -*-
# created by lilei at 2020/7/6
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello World'


@app.route('/list/')
def hello_list():
    return 'list'

if __name__ == '__main__':
    app.run('127.0.0.1', 9090)