# -*- coding: utf-8 -*-
# created by lilei at 2020/7/9
from fastapi import FastAPI

app = FastAPI()

@app.get('/')
def hello():
    return {'hello': 'world'}


